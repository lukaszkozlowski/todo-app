package gitlab.com.lukaszkozlowski.todoapp.model.event;

import gitlab.com.lukaszkozlowski.todoapp.model.Task;

import java.time.Clock;

public class TaskDone extends TaskEvent {
    public TaskDone(final Task source) {
        super(source.getId(), Clock.systemDefaultZone());
    }
}
