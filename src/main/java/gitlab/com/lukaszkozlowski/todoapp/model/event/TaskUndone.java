package gitlab.com.lukaszkozlowski.todoapp.model.event;

import gitlab.com.lukaszkozlowski.todoapp.model.Task;

import java.time.Clock;

public class TaskUndone extends TaskEvent {
    public TaskUndone(Task source) {
        super(source.getId(), Clock.systemDefaultZone());
    }
}
