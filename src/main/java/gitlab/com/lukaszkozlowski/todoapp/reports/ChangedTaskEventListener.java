package gitlab.com.lukaszkozlowski.todoapp.reports;

import gitlab.com.lukaszkozlowski.todoapp.model.event.TaskDone;
import gitlab.com.lukaszkozlowski.todoapp.model.event.TaskEvent;
import gitlab.com.lukaszkozlowski.todoapp.model.event.TaskUndone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class ChangedTaskEventListener {
    public static final Logger logger = LoggerFactory.getLogger(ChangedTaskEventListener.class);

    private final PersistedTaskEventRepository repository;

    public ChangedTaskEventListener(PersistedTaskEventRepository repository) {
        this.repository = repository;
    }
    @Async
    @EventListener
    void on(TaskDone event){
        onChanged(event);
    }
    @Async
    @EventListener
    void on(TaskUndone event){
        onChanged(event);
    }

    private void onChanged(final TaskEvent event) {
        logger.info("Got "+ event);
        repository.save(new PersistedTaskEvent(event));
    }
}
