package gitlab.com.lukaszkozlowski.todoapp.logic;

import gitlab.com.lukaszkozlowski.todoapp.TaskConfigurationProperties;
import gitlab.com.lukaszkozlowski.todoapp.model.ProjectRepository;
import gitlab.com.lukaszkozlowski.todoapp.model.TaskGroupRepository;
import gitlab.com.lukaszkozlowski.todoapp.model.TaskRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class LogicConfiguration {
    @Bean
    ProjectService projectService(
            final ProjectRepository repository,
            final TaskGroupRepository taskGroupRepository,
            final TaskGroupService taskGroupService,
            final TaskConfigurationProperties config
    ) {
        return new ProjectService(repository, taskGroupRepository, taskGroupService, config);
    }

    @Bean
    TaskGroupService taskGroupService(
            final TaskGroupRepository taskGroupRepository,
            final TaskRepository taskRepository
    ) {
        return new TaskGroupService(taskGroupRepository, taskRepository);
    }
}